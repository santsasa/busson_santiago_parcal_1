﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MinotaurDamageHit : MonoBehaviour
{
    public Transform minotaurAxeHit;
    public float hitRadius;
    public LayerMask damageLayer;
    public int damageToPlayer = 1;
    
    private SpriteRenderer _spriteRenderer;
    //private MinotaurAnimator _minotaurAnimator;
    private PlayerController _playerController;
    private MinotaurEnemy _minotaurEnemy;
    public GameObject damageOnPlayer;
    
    private int life = 4;
    private int lifeCounter;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _minotaurEnemy = GetComponent<MinotaurEnemy>();
        _playerController = GetComponent<PlayerController>();
        damageOnPlayer = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(_minotaurEnemy == null ? "NULARDO":"NO NULO");
        if (!_minotaurEnemy.spRenderer.flipX)
        {
            Vector3 flipPosition = minotaurAxeHit.localPosition;
            if (minotaurAxeHit.localPosition.x < 0)
                flipPosition.x = -minotaurAxeHit.localPosition.x;

            minotaurAxeHit.localPosition = flipPosition;
        }
        else
        {
            Vector3 flipPosition = minotaurAxeHit.localPosition;
            if (minotaurAxeHit.localPosition.x > 0)
                flipPosition.x = -minotaurAxeHit.localPosition.x;

            minotaurAxeHit.localPosition = flipPosition;
        }
    }

    public void DamageHit()
    {
        Collider2D damageHitToPlayer = Physics2D.OverlapCircle(minotaurAxeHit.position, hitRadius, damageLayer);
        PlayerController player = null;
        if (!damageHitToPlayer)
            return;
        if (damageHitToPlayer.gameObject.TryGetComponent(out player))
            player.TakeDamage(damageToPlayer);
    }
    public void TakeDamageEnemy(int damage)
    {
        lifeCounter -= damage;
        if (lifeCounter <= 0)
        {
            Destroy(gameObject);
        }
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(minotaurAxeHit.position, hitRadius);
    }
}
