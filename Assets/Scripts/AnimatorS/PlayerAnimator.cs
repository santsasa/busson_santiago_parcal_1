﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    int attackAnimation;
    private const string XAXIS = "Horizontal";

    private PlayerController playerController;

    private Animator animator;
    private new Rigidbody2D rigidbody;
    private SpriteRenderer spRenderer;

    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<PlayerController>();

        animator = playerController.animator;
        rigidbody = playerController.rigidbody;
        spRenderer = playerController.spRenderer;

        attackAnimation = Random.Range(0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        //WALK
        if (Input.GetAxis(XAXIS) != 0) //WALK
            animator.SetBool("Walking", true);

        else
            animator.SetBool("Walking", false);


        //JUMP
        if (rigidbody.velocity.y > 0)
        {
            animator.SetBool("jump", true);
        }
        else
            animator.SetBool("jump", false);

        //FALL
        if (rigidbody.velocity.y < 0)
        {
            animator.SetBool("fall", true);
        }
        else if (playerController.isGround)
            animator.SetBool("fall", false);

        int attackAnimation = Random.Range(0, 2);

        if (Input.GetKeyDown(KeyCode.Mouse0)) //Attack: two animations
        {
            if (attackAnimation == 0)
                animator.SetTrigger("attack_one");

            else if (attackAnimation == 1)
                animator.SetTrigger("attack_two");
        }
        
        //DIE
        
    }
}
