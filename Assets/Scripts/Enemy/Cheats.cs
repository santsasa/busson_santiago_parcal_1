﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheats : MonoBehaviour
{
    
    public GameObject playerToDie;
    public SpriteRenderer trampSP;
    public static bool trampState;
    private List<GameObject> ChildsTramps = new List<GameObject>();

    private void Awake()
    {
        trampState = false;

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(trampState);
            ChildsTramps.Add(child.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerToDie = GameObject.Find("Player");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trampState)
        {
            foreach (GameObject c in ChildsTramps)
                c.SetActive(trampState);
        }
        else
        {
            foreach (GameObject c in ChildsTramps)
                c.SetActive(trampState);
        }
    }

    
    
}
