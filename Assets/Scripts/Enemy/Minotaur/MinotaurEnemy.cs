﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MinotaurEnemy : MonoBehaviour
{
#region Private Seralize

    [SerializeField]
    private PlayerController target;
    [SerializeField]
    private float fieldOfView;

#endregion

#region Hide In Spector

    [HideInInspector]
    public new Rigidbody2D rigidbody;
    [HideInInspector]
    public Transform targetTransform;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public SpriteRenderer spRenderer;

#endregion

    public float speed;
    public float limitRange;
    public float attackRange;
    private int life = 13;
    private int lifeCounter;

    private void Awake()
    {
        lifeCounter = life;
    }

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        targetTransform = target.transform;
        spRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        //target = GameObject.Find("Player");
    }

    
    void Update()
    {
        if (target == null)
        {
            return;
        }
        
        spRenderer.flipX = targetTransform.position.x < transform.position.x;

        float distanceToFollowPlayer = Vector2.Distance(targetTransform.position, transform.position);

        if (distanceToFollowPlayer > fieldOfView)
        {
            if (distanceToFollowPlayer < fieldOfView)
            {
                anim.SetBool("Walk", true);
            
                Vector2 towardsThePlayer = targetTransform.position - transform.position;
                towardsThePlayer.Normalize();

                Debug.DrawRay(transform.position, targetTransform.localPosition, Color.blue, 0); //Raycast debug

                Vector3 moveSkeleton = transform.right * towardsThePlayer;
                transform.position += moveSkeleton * speed * Time.deltaTime;
            }
            else
                anim.SetBool("Walk", false);
        }
        //Attack Player
        float distanceToAttack = Vector2.Distance(targetTransform.position, transform.position); //Enemy Range
        if (distanceToAttack < attackRange)
        {
            anim.SetBool("Walk", false);
            /*int randomEnemyAttack = Random.Range(0, 101);

            if (randomEnemyAttack <= 40) //Probality: 40%
                anim.SetTrigger("Attack1");
            else if (randomEnemyAttack >= 40 && randomEnemyAttack <= 79) //Probality: 40%
                anim.SetTrigger("Attack2");
            else if (randomEnemyAttack >= 80) //Probality: 20%
            {
                anim.SetTrigger("StrongAttack");
                //Debug.Log($"Minotaur Strong Attack");
            }*/
            anim.SetTrigger("StrongAttack");

        }

    }
    
    public void TakeDamageEnemy(int damage)
    {
        lifeCounter -= damage;
        if (lifeCounter <= 0)
        {
            Destroy(gameObject);
            Debug.Log("You Win");
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color=Color.black;
        Gizmos.DrawWireSphere(transform.position, fieldOfView);

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
