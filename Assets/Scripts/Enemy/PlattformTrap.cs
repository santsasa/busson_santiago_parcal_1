﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlattformTrap : MonoBehaviour
{
    Rigidbody2D rgbd;

    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Player"))//gameObject: 
            Invoke("DropPlattform", 0.1f);

        if (collision.gameObject.layer == LayerMask.NameToLayer("Cheat_Enemy"))
            Destroy(gameObject);
        
    }
    
  
    public void DropPlattform()
    {
        rgbd.isKinematic = false;
    }
    
}
