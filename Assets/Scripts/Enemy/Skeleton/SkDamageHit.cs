﻿using System;
using UnityEngine;

public class SkDamageHit : MonoBehaviour
{
#region Public

    public Transform skeletonHit;
    public float hitRadius;
    public LayerMask damageLayer;
    public int damageToPlayer = 1;

#endregion
    
    //public LayerMask playerLayer;

#region Private

    private SpriteRenderer spRenderer;
    private SkeletonAnimator skAnim;
    private SkeletonEnemy skeletonEnemy;
    private PlayerController playercontroller;
    [HideInInspector] public GameObject playerToDamage;

#endregion

    void Awake()
    {
        //lifeCounter = life;
    }

    void Start()
    {
        spRenderer = GetComponent<SpriteRenderer>();
        skeletonEnemy = GetComponent<SkeletonEnemy>();
        skAnim = GetComponent<SkeletonAnimator>();
        playercontroller = GetComponent<PlayerController>();
        playerToDamage = GameObject.Find("Player");
        //Debug.Log(player.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (!skeletonEnemy.spRenderer.flipX)
        {
            Vector3 flipPosition = skeletonHit.localPosition;
            if (skeletonHit.localPosition.x < 0)
                flipPosition.x = -skeletonHit.localPosition.x;

            skeletonHit.localPosition = flipPosition;
        }
        else
        {
            Vector3 flipPosition = skeletonHit.localPosition;
            if (skeletonHit.localPosition.x > 0)
                flipPosition.x = -skeletonHit.localPosition.x;

            skeletonHit.localPosition = flipPosition;
        }
        
    }

    public void DamageHit()
    {
        Collider2D damageHitToPlayer = Physics2D.OverlapCircle(skeletonHit.position, hitRadius, damageLayer);
        PlayerController player = null;
        if (!damageHitToPlayer)
            return; //Rompe el método (Tipo el Break)
        
        if (damageHitToPlayer.gameObject.TryGetComponent(out player))
            player.TakeDamage(damageToPlayer);
    }
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(skeletonHit.position, hitRadius);
    }
}
