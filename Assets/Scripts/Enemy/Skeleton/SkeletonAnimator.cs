﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnimator : MonoBehaviour
{
    private SkeletonEnemy Skeleton;

    private Animator anim;
    private new Rigidbody2D rigidbody;
    private SpriteRenderer spRenderer;

    private SkeletonEnemy skeletonEnemy;

   
    void Start()
    {
        skeletonEnemy = GetComponent<SkeletonEnemy>();

        anim = skeletonEnemy.anim;
        rigidbody = skeletonEnemy.rigidbody;
        spRenderer = skeletonEnemy.spRenderer;
    }


    void Update()
    {
        //Walk
        if (rigidbody.velocity.x > 0)
            anim.SetBool("SkeletonWalk",true);
        else
            anim.SetBool("SkeletonWalk",false);
        
        

    }

    public void Attack()
    {
        
    }
}
