﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using Unity.Collections;
using UnityEngine;

public class SkeletonEnemy : MonoBehaviour
{
    public float speed;

#region Private Seraliaze

    [SerializeField]
    private PlayerController target;
    [SerializeField]
    private float fieldOfView;

#endregion

#region Hide In Spector

    [HideInInspector]
    public new Rigidbody2D rigidbody;
    [HideInInspector]
    public Transform targetTransform;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public SpriteRenderer spRenderer;

#endregion

    public SkeletonAnimator skeletonAnimatorScript;
    public float limitRange; //Enemy limit//para que el enemigo no rebote contra el Player
    public float attackRange; //Check if Player is in Enemy range
    private int life = 5;
    private int lifeCounter;

    private void Awake()
    {
        lifeCounter = life;
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        targetTransform = target.transform;
        spRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        skeletonAnimatorScript = GetComponent<SkeletonAnimator>();
        //target = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }    
        spRenderer.flipX = target.transform.position.x < transform.position.x;

        float distanceToFollowPlayer = Vector2.Distance(targetTransform.position, transform.position);

        if (distanceToFollowPlayer > limitRange)
        {

            if (distanceToFollowPlayer < fieldOfView)
            {
                anim.SetBool("SkeletonWalk", true);

                Vector2 towardsThePlayer = targetTransform.position - transform.position;
                towardsThePlayer.Normalize();

                Debug.DrawRay(transform.position, targetTransform.localPosition, Color.blue, 0); //Raycast debug

                Vector3 moveSkeleton = transform.right * towardsThePlayer;
                transform.position += moveSkeleton * speed * Time.deltaTime;
            }
            else
                anim.SetBool("SkeletonWalk",false);
        }
        //Attack Player
        float distanceToAttack = Vector2.Distance(targetTransform.position, transform.position); //Enemy range 

        if (distanceToAttack < attackRange)
        {
            anim.SetTrigger("SkAttack");
            anim.SetBool("SkeletonWalk", false);
        }
    }
    public void TakeDamageEnemy(int damage)
    {
        lifeCounter -= damage;
        if (lifeCounter <= 0)
        {
            anim.SetTrigger("Skeleton_Die");
            Destroy(gameObject);
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, fieldOfView);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
