﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SK2HitPoint : MonoBehaviour
{
 #region Public

    public Transform skeletonHit;
    public float hitRadius;
    public LayerMask damageLayer;
    public int damageToPlayer = 1;

#endregion
    
    //public LayerMask playerLayer;

 #region Private

    private SpriteRenderer spRenderer;
    private SkeletonAnimator skAnim;
    private Skeleton2 skeletonTwo;
    private PlayerController playercontroller;
    [HideInInspector] public GameObject playerToDamage;

#endregion

    void Awake()
    {
        //lifeCounter = life;
    }

    void Start()
    {
        spRenderer = GetComponent<SpriteRenderer>();
        skeletonTwo = GetComponent<Skeleton2>();
        skAnim = GetComponent<SkeletonAnimator>();
        playercontroller = GetComponent<PlayerController>();
        playerToDamage = GameObject.Find("Player");
        //Debug.Log(player.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (!skeletonTwo.spRenderer.flipX)
        {
            Vector3 flipPosition = skeletonHit.localPosition;
            if (skeletonHit.localPosition.x < 0)
                flipPosition.x = -skeletonHit.localPosition.x;

            skeletonHit.localPosition = flipPosition;
        }
        else
        {
            Vector3 flipPosition = skeletonHit.localPosition;
            if (skeletonHit.localPosition.x > 0)
                flipPosition.x = -skeletonHit.localPosition.x;

            skeletonHit.localPosition = flipPosition;
        }
        
    }

    public void DamageHit()
    {
        Collider2D damageHitToPlayer = Physics2D.OverlapCircle(skeletonHit.position, hitRadius, damageLayer);
        PlayerController player = null;
        if (!damageHitToPlayer)
        {
            return; //Rompe el método (Tipo el Break)
        }
        if (damageHitToPlayer.gameObject.TryGetComponent(out player))
        {
            player.TakeDamage(damageToPlayer);
        }
    }
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(skeletonHit.position, hitRadius);
    }
}