﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    [SerializeField]
    private Transform target = null;

    [SerializeField]
    private Vector2 minCamera;
    [SerializeField]
    private Vector2 maxCamera;

    [SerializeField]
    private Vector2 playerOfset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
 
    }

    private void LateUpdate()
    {
        if (target != null)
        {
            Vector2 newPosition =(Vector2) target.position + playerOfset;
            transform.position = newPosition;
        }
        //hacer camera limits
        
    }

    private void OnDrawGizmos()
    {
        Vector2 UPLeft = new Vector2(minCamera.x, maxCamera.y);
        Vector2 UPRight = new Vector2(maxCamera.x, maxCamera.y);

        Vector2 DownLeft = new Vector2(minCamera.x, minCamera.y);
        Vector2 DownRight = new Vector2(maxCamera.x, minCamera.y);

        Gizmos.DrawLine(UPLeft, UPRight);
        Gizmos.DrawLine(UPRight, DownRight);
        Gizmos.DrawLine(DownRight, DownLeft);
        Gizmos.DrawLine(DownLeft, UPLeft);
    }
}
