﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

#region Public

    public float speed;
    public float jumpForce;
    public float maxVelocity;
    public int damageToEnemies = 1;

    
    public int playerLife;
    [HideInInspector]
    public new Rigidbody2D rigidbody;
    [HideInInspector]
    public SpriteRenderer spRenderer;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public Transform groundChecker;
    [HideInInspector]
    public bool isGround;

    public GameObject trapEnd;
    public GameObject skeletonHit;
    
    public float groundRadius = 1;
    public LayerMask layerGround = default;
#endregion

#region Private

    private int lifeCounter;
    private int jumpsAmount;

    private SkDamageHit _skDamageHit;
    private const string XAXIS = "Horizontal"; //Variable constante, Para no escribir "Horizontal".

#endregion

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
        spRenderer = GetComponent<SpriteRenderer>();
        lifeCounter = playerLife;
        _skDamageHit = GetComponent<SkDamageHit>();
        skeletonHit = GameObject.Find("Damage Hit");
    }

    // Update is called once per frame
    void Update()
    {
        isGround = Physics2D.OverlapCircle(groundChecker.position, groundRadius, layerGround); //Ground Checker
        
        //Character movement
        if (Mathf.Abs(rigidbody.velocity.x)>maxVelocity) 
        {
            rigidbody.velocity = new Vector2(Mathf.Sign(rigidbody.velocity.x) * maxVelocity, rigidbody.velocity.y);
        }
        Vector2 velocity = Vector2.right * speed * Input.GetAxis(XAXIS);
        rigidbody.AddForce(velocity);


        //FLIP CHARACTER IN X
        if (Input.GetAxis(XAXIS) < 0)
            spRenderer.flipX = true;

        else if (Input.GetAxis(XAXIS) > 0)
            spRenderer.flipX = false;


        //Jumps
        if (jumpsAmount > 0 && Input.GetButtonDown("Jump"))
        {
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            //animator.SetBool("jump", true);
            jumpsAmount--;
        }

        if (isGround) //resetting jumps
        {
            jumpsAmount = 1;
            //animator.SetBool("jump", false);
        }
    }

    public void LifeBuffo(int increase)
    {
        lifeCounter += increase;
    }

    public void TakeDamage(int damage)
    {
        lifeCounter -= damage;
        Debug.Log($"Te queda {lifeCounter} vidas");
        if (lifeCounter <= 0)
            Loser();
    }

    /*public void DamageToEnemies(int d)
    {
        damageToEnemies += d;
    }*/
    
    private void OnCollisionEnter2D(Collision2D trampToInstaDie)//Tramp over 
    {
        if (trampToInstaDie.gameObject.layer == LayerMask.NameToLayer("Cheat_Enemy"))
        {
            Cheats.trampState = true;
            Invoke("Loser", 0f);
        }
    }
    
    
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundChecker.position, groundRadius); //Check ground
    }

    public void Loser()
    {
        Debug.Log($"You Lose");
        Destroy(gameObject);
    }
}
