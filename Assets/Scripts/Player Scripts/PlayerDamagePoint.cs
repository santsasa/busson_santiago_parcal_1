﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerDamagePoint : MonoBehaviour
{
    public Transform damagePoint;
    public float damageRadius;
    public LayerMask layerEnemy;
    public int damageToEnemy = 1;
    
    private PlayerController playerController;
    private SkeletonEnemy _skeletonEnemy;
    private MinotaurEnemy _minotaurEnemy;

    void Start()
    {
        playerController = GetComponent<PlayerController>();
        _minotaurEnemy = GetComponent<MinotaurEnemy>();
        _skeletonEnemy = GetComponent<SkeletonEnemy>();
    }

    public void PDamage() //Player damage
    {
        Collider2D[] enemyHit= Physics2D.OverlapCircleAll(damagePoint.position, damageRadius, layerEnemy);
        MinotaurEnemy minotaur = null;
        SkeletonEnemy skeleton = null;
        Skeleton2 skeleton2 = null;
        for (int i = 0; i < enemyHit.Length; i++)
        {
            Debug.Log($"Hit to {enemyHit[i].gameObject.name}");
            if (!enemyHit[i])
                return;
            
            if (enemyHit[i].gameObject.TryGetComponent(out minotaur))
                minotaur.TakeDamageEnemy(damageToEnemy); 

            if (enemyHit[i].gameObject.TryGetComponent(out skeleton))
                skeleton.TakeDamageEnemy(damageToEnemy);

            if (enemyHit[i].gameObject.TryGetComponent(out skeleton2))
                skeleton2.TakeDamageEnemy(damageToEnemy); 
        }
        
    }

    void Update()
    {
        if (!playerController.spRenderer.flipX) //Flipx Sword
        {
            Vector3 flipPosition = damagePoint.localPosition;
            if (damagePoint.localPosition.x < 0)
                flipPosition.x = -damagePoint.localPosition.x;

            damagePoint.localPosition = flipPosition;
        }
        else
        {
            Vector3 flipPosition = damagePoint.localPosition;
            if (damagePoint.localPosition.x > 0)
                flipPosition.x = -damagePoint.localPosition.x;

            damagePoint.localPosition = flipPosition;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue; //Draw Damage radius
        Gizmos.DrawWireSphere(damagePoint.position, damageRadius);
    }
}
