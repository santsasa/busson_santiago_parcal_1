﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public GameObject isDead;
    public SpriteRenderer diamond;
    public GameObject player;
    public bool diamondState;
    

    private void Awake()
    {
        diamondState = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            gameObject.SetActive(false);
        }
        else if (!isDead)
        {
            gameObject.SetActive(true);
        }
    }
}
